.. commons-python documentation master file, created by
   sphinx-quickstart on Sat Mar 12 12:26:25 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ufl-python's documentation!
==========================================

.. toctree::
    :maxdepth: 3

    apidocs/core.rst
    apidocs/io.rst
    apidocs/os.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

