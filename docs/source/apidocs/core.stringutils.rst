======================================
Primitive String Templating Extensions
======================================

.. automodule:: ufl.core.stringutils
    :members:
