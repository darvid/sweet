===============================================
Data Structure Abstractions and Implementations
===============================================

.. automodule:: ufl.core.structures

.. automodule:: ufl.core.structures.dict
    :members:

.. automodule:: ufl.core.structures.enum
    :members:

.. automodule:: ufl.core.structures.iterable
    :members:

.. automodule:: ufl.core.structures.list
    :members:
