====================
Operating System API
====================

.. note::

    Some of the libraries included with the Python implementation of UFL may
    not be included in the official UFL specification.

.. automodule:: ufl.os

Contents:

.. toctree::
    :maxdepth: 2

    os.xlib
