========
Core API
========

.. note::

    Some of the libraries included with the Python implementation of UFL may
    not be included in the official UFL specification.

.. automodule:: ufl.core

Contents:

.. toctree::
    :maxdepth: 2

    core.funcutils
    core.structures
    core.stringutils

.. autofunction:: ufl.core.findattr
