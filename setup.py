#!/usr/bin/env python
from setuptools import setup
from sweet import __doc__, __version__
setup(
    name = "sweet",
    version = __version__,
    packages = [
        "sweet",
        "sweet.io",
        "sweet.structures",
        "sweet.graphics",
    ],
    description = "".join(map(str.strip, __doc__.split("\n")[4:6])),
    long_description = __doc__,
    maintainer = "David Gidwani",
    maintainer_email = "david.gidwani AT gmail DOT com",
    url = "http://dave.uni.cx/project/sweet",
    keywords = ["sweet", "general-purpose"],
    classifiers = [
        "Programming Language :: Python",
        "Development Status :: 1 - Planning",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries",
    ]
)
