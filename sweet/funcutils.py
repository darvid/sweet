"""
    sweet.core.funcutils
    ~~~~~~~~~~~~~~~~~~

    Provides various decorators and function helpers.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
from functools import wraps

__all__ = ["memoized", "cached_property", "to_list"]


class Memoizer(object):

    def __init__(self, func):
        self.func = func
        self.cache = {}

    def __call__(self, *args):
        try:
            return self.cache[args]
        except KeyError:
            self.cache[args] = value = self.func(*args)
            return value
        except TypeError:
            return self.func(*args)

    def __repr__(self):
        return self.func.__doc__


def memoized(func):
    """Memoize the result of a callable."""
    return Memoizer(func)


def cached_property(func):
    """Cache a property."""
    @wraps(func)
    def decorator(cls):
        try:
            return cls.__cache[func]
        except AttributeError:
            cls.__cache = {}
        except KeyError:
            pass
        cls.__cache[func] = func(cls)
        return cls.__cache[func]
    return property(fget=decorator)


def to_list(func):
    """Wrap function result with `list`."""
    @wraps(func)
    def decorator(*args, **kwargs):
        return list(func(*args, **kwargs))
    return decorator
