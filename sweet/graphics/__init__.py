"""
    sweet.core.graphics
    ~~~~~~~~~~~~~~~~~

    Provides color abstraction and other imaging related utilities.

    :copyright: Copyright (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
