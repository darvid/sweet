"""
    sweet
    ~~~

    A collection of general purpose libraries for Python.

    :copyright: Copyright 2009 UFL team. See AUTHORS for details.
    :license: GPLv3
"""

__version__ = "0.1-pre"
