"""
    sweet.core.stringutils
    ~~~~~~~~~~~~~~~~~~~~

    Provides string formatters and encapsulates various standard library
    string manipulation procedures.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
import ast
import string


__all__ = [
    "SafeObjectFormatter",
    "render_template"
]


class SafeObjectFormatter(string.Formatter):
    """A :class:`string.Formatter` that supports accessing attributes, indexes,
       and public methods:

        >>> f = SafeObjectFormatter()
        >>> f.format("{foo.upper()}", foo="bar")
        'BAR'
        >>> f.format("{1.2}", "foo", [1,2,3])
        '3'
    """

    def get_field(self, field, args, kwargs):

        def _get_field_obj(key):
            chunks = key.split(".")
            if chunks[0].isdigit() and (len(args) - 1) >= int(chunks[0]):
                obj = args[int(chunks[0])]
            else:
                obj = kwargs[chunks[0]]
            return (chunks, obj)

        try:
            return super(SafeObjectFormatter, self).get_field(
                field, args, kwargs)
        except AttributeError:
            chunks, obj = _get_field_obj(field)
            # Unsafe, swiss cheese, microsoft version:
            # result = eval(field, {"__builtins__": None}, kwargs)
            # Safer, albeit limited version:
            for c in chunks[1:]:
                lp_pos = c.find("(")
                arguments = ()
                if lp_pos != -1:
                    str_arguments = c[lp_pos+1:c.find(")")]
                    if str_arguments:
                        arguments = ast.literal_eval(str_arguments)
                        if type(arguments) != tuple and c[lp_pos+1] != "(":
                            arguments = (arguments, )
                    c = c[:lp_pos]
                assert not c.startswith("_")
                if c.isdigit() and not arguments:
                    obj = obj[int(c)]
                else:
                    obj = getattr(obj, c)(*arguments)
            return (obj, chunks[0])


def render_template(template_path, output_path=None, numbered_data=[],
    named_data={}):
    """Render a template using `SafeObjectFormatter`.

    :param output_path: If specified, renders template to a file rather
                        returning the rendered string.
    """
    formatter = SafeObjectFormatter()
    with open(template_path, "r+") as f:
        data = formatter.format(f.read(), *numbered_data, **named_data)
        return open(output_path, "w").write(data) if output_path else data
