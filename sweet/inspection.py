"""
    sweet.inspection
    ~~~~~~~~~~~~~~~~

    Provides assorted object inspection and introspection utilities.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
__all__ = ["findattr", "get_subclasses", "get_subclass_by_name"]


def findattr(obj, *attributes):
    """Return the first attribute in *obj* that exists.

        >>> findattr(str, "foo", "bar", "lower")
        <method 'lower' of 'str' objects>
    """
    try:
        return getattr(obj, attributes[0])
    except AttributeError as err:
        if not attributes[1:]:
            raise err
        return findattr(obj, *attributes[1:])


def get_subclasses(superclass):
    """Return all classes that inherit a given *superclass*."""
    subclasses = set()
    inspect = [superclass]
    while inspect:
        parent = inspect.pop()
        for child in parent.__subclasses__():
            if child not in subclasses:
                subclasses.add(child)
                inspect.append(child)
    return subclasses


def get_subclass_by_name(superclass, name):
    result = [cls for cls in get_subclasses(superclass) if cls.__name__ == name]
    if result:
        return result[0]