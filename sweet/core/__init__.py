"""
    sweet.core
    ~~~~~~~~

    Provides core modules and libraries: data structure abstractions, image and
    color management and manipulation, and various utilities.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
def findattr(obj, *attributes):
    """Return the first attribute in *obj* that exists."""
    try:
        return getattr(obj, attributes[0])
    except AttributeError as err:
        if not attributes[1:]:
            raise err
        return findattr(obj, *attributes[1:])


def get_subclasses(superclass):
    """Return all classes that inherit a given *superclass*."""
    subclasses = set()
    inspect = [superclass]
    while inspect:
        parent = inspect.pop()
        for child in parent.__subclasses__():
            if child not in subclasses:
                subclasses.add(child)
                inspect.append(child)
    return subclasses


def get_subclass(superclass, name):
    result = [cls for cls in get_subclasses(superclass) if cls.__name__ == name]
    if result:
        return result[0]


def import_(statement, suppressed=False):
    """Import a module or object(s) from a module given a **statement** like::

        package[.subpackage[:object(, object, ...)]]

    For example, to import `choice` and `randint` from :mod:`re`::

        >>> immigrate('random:choice,randint') # doctest: +ELLIPSIS
        ... # doctest: +NORMALIZE_WHITESPACE
        [<bound method Random.choice of <random.Random object at 0x...>>,
         <bound method Random.randint of <random.Random object at 0x...>>]

    :param suppressed: If **True**, suppresses any exceptions that are thrown
                       during the import.
    """
    try:
        if ":" in statement:
            pak, obj = statement.split(':')
            if "," in obj:
                obj = list(map(str.strip, obj.split(',')))
            else:
                obj = [obj]
            mod = __import__(pak, fromlist=obj)
            if len(obj) == 1:
                obj = obj[0]
                if obj == "*":
                    try:
                        attrs = mod.__all__
                    except AttributeError:
                        attrs = dir(mod)
                    return [getattr(mod, attr) for attr in attrs]
                else:
                    return getattr(mod, obj)
            else:
                return [getattr(mod, o) for o in obj]
        else:
            mod = __import__(statement)
            for submod in statement.split(".")[1:]:
                mod = getattr(mod, submod)
            return mod
    except Exception:
        if not suppressed:
            raise