"""
    sweet.core.structures
    ~~~~~~~~~~~~~~~~~~~

    Implements commonly used data structures, extends existing ones, and
    provides related accessory functions.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
from .dict import *
from .enum import *
from .iterable import *
from .list import *