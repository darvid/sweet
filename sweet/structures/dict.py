"""
    sweet.core.structures.dict
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Provides some useful variations of the dictionary structure, as well as
    commonly used dict-related procedures.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
try:
    from collections import OrderedDict, Iterable
except ImportError:
    from ordereddict import OrderedDict
    Iterable = None

from sweet.structures.list import flatten


__all__ = [
    "find_key",
    "find_key_with_parents",
    "map_dict",
    "reverse_dict",
    "AttrDictMixin",
    "AttrDict",
    "DeepAttrDictMixin",
    "DictNamespaceMixin"
]


def reverse_dict(dictionary):
    """Reverse dictionary keys with values."""
    return dict((v, k) for k, v in dictionary.items())


def map_dict(function, dictionary, map_keys=False, map_values=True):
    """Map a function to dictionary keys and/or values."""
    return dict([
        (function(k) if map_keys else k, function(v) if map_values else v)
        for k, v in  dictionary.items()
    ])


def find_key(name, dictionary):
    """Find the first occurence of *name* (as a key) in a dictionary."""
    stack = [dictionary]
    while stack:
        item = stack.pop()
        if name in item:
            return item[name]
        for key, value in item.items():
            if isinstance(value, dict):
                stack.append(value)


def find_key_with_parents(name, dictionary):
    """Return the first occurence of *name* and its parents in a dictionary."""
    parents = []
    for key, value in dictionary.items():
        if key == name:
            return [key]
        if isinstance(value, dict):
            parents.append(flatten([key, find_key_with_parents(name, value)]))
            if name in parents[-1]: return parents[-1]


class AttrDictMixin(object):
    """Expose attribute style access to any dict-like structure.

        >>> class Foo(AttrDictMixin, dict):
        ...     pass
        >>> f = Foo()
        >>> f['bar'] = 'baz'
        >>> f
        {'bar': 'baz'}
        >>> f.bar
        'baz'
    """

    def __init__(self, *args, **kwargs):
        if args:
            m = args[0]
            if isinstance(m, dict):
                m = m.items()
        elif kwargs:
            m = kwargs.items()
        else:
            m = []
        if m:
            for k, v in m:
                self[k] = v

    def __expand(self, key, value):
        if '.' in key:
            self.pop(key, None)
            parts = key.split('.')
            return {parts[0]: self.__expand('.'.join(parts[1:]), value)}
        else:
            return {key: value}

    def copy(self):
        return self.__copy__()

    def __delattr__(self, key):
        try:
            super(AttrDictMixin, self).__delattr__(key)
        except AttributeError:
            self.__delitem__(key)

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError as k:
            if key.endswith("_"):
                try:
                    return self[key[:-1]]
                except KeyError:
                    pass
            raise AttributeError(k)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        if "." in key:
            parts = key.split(".")
            self.__setitem__(parts[0], self.__expand(".".join(parts[1:]),
                value))
        else:
            super(AttrDictMixin, self).__setitem__(key, value)

    def __copy__(self):
        return AttrDict(self)


class DeepAttrDictMixin(AttrDictMixin):
    """Recursively create :class:`AttrDict` for nested dictionaries."""

    def __getattr__(self, key):
        if Iterable is not None:
            is_iterable = isinstance(self[key], Iterable)
        else:
            is_iterable = isinstance(self[key], (list, tuple))
        if is_iterable:
            for index, value in enumerate(self[key]):
                if isinstance(value, dict):
                    self[key][index] = AttrDict(value)
        return super(DeepAttrDictMixin, self).__getattr__(key)

    def __setitem__(self, key, value):
        if isinstance(value, dict):
            value = AttrDict(value)
        super(DeepAttrDictMixin, self).__setitem__(key, value)


class AttrDict(DeepAttrDictMixin, dict):
    """A dictionary with attribute/dotted style access."""
    pass


class OrderedAttrDictMixin(DeepAttrDictMixin):
    """An ordered dictionary with attribute/dotted style access."""

    def __init__(self, *args, **kwargs):
        object.__setattr__(self, "_data", OrderedDict(*args, **kwargs))

    def keys(self):
        return object.__getattribute__(self, "_data").keys()

    def values(self):
        return object.__getattribute__(self, "_data").values()

    def items(self):
        return object.__getattribute__(self, "_data").items()

    def __iter__(self):
        return object.__getattribute__(self, "_data").__iter__()

    def __getitem__(self, key):
        return object.__getattribute__(self, "_data").__getitem__(key)

    def __setitem__(self, key, value):
        self._data.__setitem__(key, value)

    def __repr__(self):
        return repr(self._data)


class DeepOrderedAttrDictMixin(OrderedAttrDictMixin):
    """Recursively create nested :class:`~collections.OrderedDict` on setitem."""

    def __setitem__(self, key, value):
        if isinstance(value, dict):
            value = OrderedAttrDict(value)
        super(DeepOrderedAttrDictMixin, self).__setitem__(key, value)


class OrderedAttrDict(DeepOrderedAttrDictMixin, dict):
    """An ordered dictionary with attribute/dotted style access."""
    pass


class DictNamespaceMixin(object):
    """Prefix all keys with a specified namespace on item manipulation.

    Set the namespace class property in order for this to work::

        >>> class FooDict(DictNamespaceMixin, dict):
        ...    namespace = 'foo'
        >>> foo = FooDict({'bar': 'baz'})
        >>> foo
        {'foo.bar': 'baz'}
        >>> foo['bar']
        'baz'
    """

    namespace = ''
    delimiter = '.'

    def __init__(self, *args, **kwargs):
        if args:
            m = args[0]
            if type(m) == dict:
                m = m.items()
        elif kwargs:
            m = kwargs.items()
        else:
            m = []
        if m:
            for k, v in m:
                self[k] = v

    def __delitem__(self, key):
        dict.__delitem__(self,
            self.delimiter.join((self.namespace, key)))

    def __getitem__(self, key):
        return dict.__getitem__(self,
            self.delimiter.join((self.namespace, key)))

    def __setitem__(self, key, value):
        dict.__setitem__(self,
            self.delimiter.join((self.namespace, key)), value)


class DictKeySynonymMixin(object):
    """Map one or more synonyms to particular keys in a dictionary.

    `__synonyms__` should be a **dict** consisting of key-to-synonyms pairs.
    Synonyms may be a single string, or an iterable consisting of multiple
    synonyms.

    :note: `__synonyms__` must be a **class attribute**, *not* an instance
    attribute.
    """
    __synonyms__ = []

    def __init__(self, *args, **kwargs):
        super(DictKeySynonymMixin, self).__init__(*args, **kwargs)
        self._synonym_map = {}
        for key, value in self.items():
            for synonyms in self.__synonyms__:
                if key in synonyms:
                    self._add_synonyms(synonyms, key)

    def _add_synonyms(self, synonyms, key):
        for synonym in synonyms:
            if synonym != key:
                self._synonym_map[synonym] = key

    def __delitem__(self, key):
        for synonym, key_ in self._synonym_map.items():
            if key_ == key:
                del self._synonym_map[key_]
        super(DictKeySynonymMixin, self).__delitem__(key)

    def __setitem__(self, key, value):
        for synonyms in self.__synonyms__:
            if key in synonyms:
                self._add_synonyms(synonyms, key)
        super(DictKeySynonymMixin, self).__setitem__(key, value)

    def __getitem__(self, key):
        try:
            return super(DictKeySynonymMixin, self).__getitem__(key)
        except KeyError:
            return self[self._synonym_map[key]]
