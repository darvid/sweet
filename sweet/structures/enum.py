"""
    sweet.core.structures.enum
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Provides structure types similar to *enums* found in many other languages.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict
from types import MethodType

from sweet.structures.dict import OrderedAttrDict, map_dict, reverse_dict
from functools import reduce


__all__ = [
    "simple_enum",
    "Enumeration",
    "MultiKeyEnumMixin",
    "MultiKeyEnumeration"
]


def _clean(string):
    return string.replace(" ", "_").replace("-", "_").upper()


class BitmaskEnumMixin(object):
    """Create an enumeration of bitmasks."""

    def __init__(self, *sequential, **named):
        super(BitmaskEnumMixin, self).__init__(*sequential, **named)
        for name in self:
            self[name] = 1 << self[name]

    def test(self, mask, value):
        """Test a bitmask against a bitnumber."""
        if isinstance(value, str):
            value = self[value]
        return mask & value == value

    @property
    def ALL(self):
        return reduce(lambda a, b: a | b, list(self.values()))


class MultiKeyEnumMixin(object):
    """Allow multiple keys to be associated with the same value."""

    def __init__(self, *sequential, **named):
        named = map_dict(_clean, named, map_keys=True, map_values=False)
        index = 0
        for item in sequential:
            if isinstance(item, (list, tuple)):
                for subitem in item:
                    named[_clean(subitem)] = index
            else:
                named[_clean(item)] = index
            index += 1
        super(MultiKeyEnumMixin, self).__init__(**named)


class Enumeration(OrderedAttrDict):
    """A dictionary-based enumeration type."""

    def __init__(self, *sequential, **named):
        sequential = list(map(_clean, sequential))
        named = map_dict(_clean, named, map_keys=True, map_values=False)
        super(Enumeration, self).__init__(**OrderedDict(list(zip(sequential,
            list(range(len(sequential))))), **named))

        def setitem(cls, key, value):
            raise Exception("Enumerations are read-only")

        object.__setattr__(self, "__setitem__",
                           MethodType(setitem, self.__class__))

    def get(self, key):
        try:
            return self[key]
        except KeyError:
            return self[_clean(key)]

    def get_name(self, value):
        """Return the name associated with a given value."""
        return reverse_dict(self)[value]

    def __repr__(self):
        return "<Enumeration({0})>".format(list(self.keys()))


class MultiKeyEnumeration(MultiKeyEnumMixin, Enumeration):
    pass


class BitmaskEnumeration(BitmaskEnumMixin, Enumeration):
    pass


#: http://stackoverflow.com/questions/36932/#1695250
def simple_enum(*sequential, **named):
    """Create a primitive enumeration type."""
    sequential = [_clean(i) for i in sequential]
    named = map_dict(_clean, named, map_keys=True, map_values=False)
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type("Enumeration", (), enums)
