"""
    sweet.core.structures.iterable
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    Provides an extremely basic iterable implementation.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
class Iterable(object):
    """An extendable iterable base class."""

    #: The current iteration position.
    _current_index = -1

    #: The object to iterate upon.
    _iterable_target = []

    def __next__(self):
        """Iterate to the next item in the object."""
        if self._current_index >= len(self) - 1:
            raise StopIteration
        else:
            self._current_index += 1
        return self[self._current_index]

    def __getitem__(self, index):
        return self._iterable_target[index]

    def __iter__(self):
        return self

    def __len__(self):
        return len(self._iterable_target)
