"""
    sweet.core.structures.list
    ~~~~~~~~~~~~~~~~~~~~~~~~

    Provides various helper functions to lists and tuples, stuff that would be
    really nice to have in :mod:`itertools`.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
from collections import Iterable
from types import GeneratorType


def flatten(iterable):
    """Flatten a list (of any depth)."""
    if isinstance(iterable, Iterable) and not isinstance(iterable, str):
        return [sub for item in iterable for sub in flatten(item)]
    else:
        return [iterable]


def flatten_lazy(iterable):
    """A generator version of :func:`flatten`."""
    if isinstance(iterable, Iterable) and not isinstance(iterable, str):
        for item in iterable:
            for subitem in flatten_lazy(item):
                yield subitem
    else:
        yield iterable


class MultiGetList(list):

    def __getitem__(self, index):
        if isinstance(index, tuple):
            return list(map(super(MultiGetList, self).__getitem__, index))
        else:
            return super(MultiGetList, self).__getitem__(index)
