"""
    sweet.io.shell
    ~~~~~~~~~~~~

    Provides process abstractions and various shell-related utilities.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
import re
import shlex
import signal
import subprocess
from subprocess import PIPE, STDOUT

from sweet.core.structures.enum import Enumeration
from sweet.core.structures.dict import AttrDict
from sweet.io.fs import path, Path


__all__ = [
    "DISABLE_COLORS",
    "colors",
    "cprint",
    "execute",
    "states",
    "wait",
    "Command",
    "Descriptor",
    "Input",
    "Output",
    "TimeoutError",
]


DISABLE_COLORS = False
color_fmt_re = r"(?:^%([{0}])|([^%])%([{0}]))".format("krgybmcwnKRGYBMCWN")
colors = dict(list(zip(
    "krgybmcwnKRGYBMCWN",
    ["\033[0;{}m".format(i) for i in range(30, 38)] +
    ["\033[0m"] +
    ["\033[1;{}m".format(i) for i in range(30,38)] +
    ["\033[0m"]
)))
states = Enumeration(
    "created",
    "running",
    "terminated",
    "killed"
)


def cformat(string):
    """Format a string using irssi-style color variables."""
    def expand(match):
        groups = match.groups()
        seq = colors[groups[0] or groups[2]]
        return (groups[1] or "") + ("" if DISABLE_COLORS else seq)
    string = re.sub(color_fmt_re, expand, string) + colors["n"]
    return string.replace("%%", "%")


def cprint(string):
    """Print a string formatted using irssi-style color variables."""
    print((cformat(string)))


class TimeoutError(Exception):
    pass


def execute(command_line, **kwargs):
    """Equivalent to :class:`~commands.getstatusoutput`.

    :param kwargs: Passed to :class:`~subprocess.Popen`.
    :return: A tuple of (exit code, output).
    """
    kwargs["stdout"] = kwargs.get("stdout", None)
    return Command(command_line).run(**kwargs)


def wait(command_line, **kwargs):
    kwargs["stdout"] = kwargs.get("stdout", None)
    return Command(command_line).wait(**kwargs)


class Descriptor(object):
    """Represents a file descriptor."""

    def __init__(self, fd):
        self.fd = fd

    def __repr__(self):
        return "<{0}({1})>".format(self.__class__.__name__, repr(self.fd))


class Input(Descriptor):
    """Mostly a reference class. Represents file input."""

    def __gt__(self, command):
        assert not command.process
        return command.run(stdin=self.fd)

    def __lt__(self, command):
        raise TypeError("wrong pipe direction")


class Output(Descriptor):

    def __lt__(self, command):
        assert not command.process
        return command.run(stdout=self.fd)

    def __gt__(self, command):
        raise TypeError("wrong pipe direction")


class Command(object):
    """Thin wrapper around :class:`~subprocess.Popen` to allow for redirection,
       input, and basic piping.

        >>> (Command("echo foo") | Command("cat")).stdout
        'foo\n'
        >>> str(Command("cat") < "bar")
        'bar'

    The *greater-than* operator (output redirection) handles a string (an
    absolute path to a file), a :class:`~sweet.io.fs.Path` object, or an open file
    descriptor on the right-hand side.

    The *less-than* operator (input) accepts a string or an open file descriptor
    or a :class:`~sweet.io.fs.Path` object on the left-hand side.

    The *inclusive bit-or* operator (used for piping commands) allows for piping
    multiple :class:`Command` objects together, just as one would do on the
    command line.
    """
    default_options = dict(stdin=None, stdout=PIPE, stderr=PIPE)

    def __init__(self, command_line):
        self.args = shlex.split(command_line)
        self._process = None
        self.state = states.CREATED

    @property
    def pid(self):
        return self._process.pid

    @property
    def returncode(self):
        return self._process.returncode

    @property
    def stdout(self):
        return self._process.stdout

    @property
    def stdin(self):
        return self._process.stdin

    @property
    def stderr(self):
        return self._process.stderr

    def kill(self):
        if self.state == states.RUNNING:
            self._process.kill()
            self.state = states.KILLED

    def run(self, timeout=None, **kwargs):
        """Spawn and wait for process to end.

        :rtype: a :class:`Response` object.
        """
        self._create_process(**kwargs)
        if kwargs.get("stdin", None) == PIPE:
            return
        elif "stdin" in kwargs and isinstance(kwargs["stdin"], str):
            stdin = kwargs["stdin"]
        else:
            stdin = None
        if timeout:
            def handle_alarm(signum, frame):
                if self.state == states.RUNNING:
                    self.kill()
                    raise TimeoutError("process {0}'s runtime exceeded timeout "
                                       "and was killed.".format(self.pid))
            signal.signal(signal.SIGALRM, handle_alarm)
            signal.alarm(timeout)
        self.state = states.RUNNING
        # not sure if try/finally really necessary
        try:
            stdout, stderr = self._process.communicate(input=stdin)
            self._process.stdout = stdout
            self._process.stderr = stderr
        finally:
            self.state = states.TERMINATED
        return self

    def wait(self, **kwargs):
        self._create_process(**kwargs)
        self.state = states.RUNNING
        self._process.wait()
        # not sure if try/finally really necessary
        try:
            for stream in ("stdout", "stderr"):
                fd = getattr(self._process, stream)
                if fd is not None:
                    setattr(self._process, stream, fd.read())
        finally:
            self.state = states.TERMINATED
        self.state = states.TERMINATED
        return self

    def _create_process(self, secure=True, **kwargs):
        args = self.args
        if kwargs.get("shell", False):
            args = " ".join(self.args)
            if ";" in args and secure:
                raise Exception("suspected shell injection")
        if not self.state == states.RUNNING and self._process is None:
            self._process = subprocess.Popen(args, **self._get_defaults(kwargs))
            return True

    def _get_defaults(self, kwargs):
        defaults = self.default_options
        defaults.update(kwargs)
        if ("stdin" in kwargs and not (defaults["stdin"] == PIPE or
                                       hasattr(kwargs["stdin"], "write"))
            and isinstance(kwargs["stdin"], str)):
            defaults["stdin"] = PIPE
        return defaults

    def __getattr__(self, key):
        try:
            return getattr(object.__getattribute__(self, "_process"), key)
        except AttributeError:
            return object.__getattribute__(self, key)

    def __gt__(self, fd):
        if type(fd) in (str, str):
            fd = open(fd, "w+")
        elif type(fd) == Path:
            fd = open(fd.absolute, "w+")
        return self.run(stdout=fd)

    def __lt__(self, fd):
        if isinstance(fd, Path):
            fd = open(fd.absolute, "r")
        return self.run(stdin=fd)

    def __or__(self, proc2):
        if not self._process:
            self.run(stdout=PIPE)
        return proc2.run(stdin=self._process.stdout, stdout=PIPE)

    def __bool__(self):
        if self.state == states.TERMINATED:
            return self.returncode == 0
        return False

    def __repr__(self):
        return "<Command({0}, state={1})>".format(
            repr(self.args[:8] + (self.args[8:] and ["..."])),
            states.get_name(self.state)
        )