"""
    sweet.io.fs
    ~~~~~~~~~

    An object-oriented filesystem interface.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
import functools
import glob
import os
import re
import shutil


__all__ = [
    "file_local_path",
    "path",
    "Path",
    "Directory",
    "File",
    "NonexistentPath",
    "FsException",
    "NonexistentPathError"
]


class FsException(OSError):
    pass


class NonexistentPathError(FsException):
    pass


def require_exists(fn):
    def wrapper(self, *args, **kwargs):
        if self.exists:
            return fn(self, *args, **kwargs)
        else:
            raise NonexistentPathError(
                "No such file or directory: '{0}'.".format(self.absolute))
    wrapper.__name__ = fn.__name__
    wrapper.__doc__ = fn.__doc__
    return wrapper


def path(*paths):
    """A path factory.

    Given a path, return one of :class:`NonexistentPath`, :class:`File`,
    or :class:`Directory` instances."""
    paths = list(paths)
    base = paths.pop(0)
    if not isinstance(base, Path):
        base = os.path.expanduser(base)
        if os.path.exists(base):
            cls = Directory if os.path.isdir(base) else File
        else:
            cls = NonexistentPath
        base = cls(base)
    return paths and base.join(*paths) or base


def file_local_path(path_str, filename):
    """Return a file or directory relative to *filename*."""
    parent = path(os.path.dirname(os.path.realpath(filename)))
    return parent.join(path_str)


class Path(str):
    """Represents a path."""

    def __init__(self, path):
        """Construct a `Path` instance from a path string."""
        self.raw = os.path.expanduser(os.path.expandvars(path))

    @property
    def absolute(self):
        """Return an absolute path via `os.path.abspath`."""
        return os.path.abspath(self.raw)

    @property
    def chunks(self):
        return self.absolute.split('/')

    @property
    def dirname(self):
        return os.path.dirname(self.raw)

    @property
    def exists(self):
        """Return boolean indicating whether or not the path exists."""
        return os.path.exists(self.raw)

    @property
    def hidden(self):
        """Return True if the file or directory name begins with a period."""
        return True if self.name.startswith('.') else False

    @property
    def parent(self):
        """Return the parent directory of the path (if any)."""
        return Directory(os.path.split(self.absolute)[0])

    @property
    def name(self):
        return self.get_name()

    @property
    def extension(self):
        return os.path.splitext(self.name)[1]

    @property
    def is_abs(self):
        """See `os.path.isabs`."""
        return os.path.isabs(self.raw)

    @property
    def is_dir(self):
        """See `os.path.isdir`."""
        return os.path.isdir(self.raw)

    @property
    def is_file(self):
        """See `os.path.isfile`."""
        return os.path.isfile(self.raw)

    @property
    def is_link(self):
        """See `os.path.islink`."""
        return os.path.islink(self.raw)

    @property
    def is_mount(self):
        """See `os.path.ismount`."""
        return os.path.ismount(self.raw)

    @property
    @require_exists
    def stat(self):
        return os.stat(self.absolute)

    def create(self, recursive=True, as_file=False, silent=False):
        """Create a file or directory from the current path.

        :param recursive: If `True`, create a directory structure recursively
                          rather than just the leaf directory.
        :param as_file: If `True`, *touch* a file rather than creating a
                        directory.
        """
        if self.exists:
            if not silent:
                raise FsException("Path already exists")
        else:
            if as_file:
                open(self.raw, "w").write()
            else:
                return (os.makedirs if recursive else os.mkdir)(self.raw)

    def get_name(self, with_extension=True):
        p = os.path.split(self.raw)[1]
        return p if with_extension else os.path.splitext(p)[0]

    def join(self, other_path, absolute=True):
        """Return one of :class:`NonexistentPath`, :class:`File`, or
        :class:`Directory` instances.

        :param path_name: A single path or an iterable of paths to join with.
        :param absolute: Boolean indicating whether or not to join with the
            first path's absolute path.
        """
        if isinstance(other_path, Path):
            other_path = other_path.raw
        return path(os.path.realpath(os.path.join(self.absolute
            if absolute else self.raw, other_path)))

    def open(self, flags='r'):
        """Return a file descriptor."""
        return open(self.raw, flags)

    @require_exists
    def move(self, destination):
        destination = path(destination)
        shutil.move(self.absolute, destination.absolute)

    def remove(self):
        """Remove (delete) the path."""
        return os.remove(self.raw)

    def rename(self, new_name, absolute=False):
        """Rename the file or directory.

        Parent directory path is automatically added to *new_name*. Set
        *absolute* to False if you wish to prevent this.
        """
        if not absolute:
            new_name = self.parent.join(new_name).absolute
        os.rename(self.absolute, new_name)
        self.raw = new_name
        return self

    def write(self, data, mode='w'):
        """Write to a file."""
        if not self.is_dir:
            f = open(self.raw, mode)
            f.write(data)
            f.close()

    def __bool__(self):
        return self.exists

    def __repr__(self):
        return '<%s("%s")>' % (self.__class__.__name__, self.absolute)

    def __str__(self):
        return self.raw


class NonexistentPath(Path):
    """Represents a non-existant path."""


class Directory(Path):
    """Represents a directory."""

    def __init__(self, *args, **kwargs):
        super(Directory, self).__init__(*args, **kwargs)
        #: The current directory iteration index.
        self.current_index = -1

    def copy(self, destination):
        """Copy a directory tree."""
        destination = path(destination)
        return shutil.copytree(self.absolute, destination.absolute)

    @property
    def files(self):
        return [p for p in self.listing if p.is_file]

    @property
    def directories(self):
        return [p for p in self.listing if p.is_dir]

    def find_all(self, pattern, exclude=[], recurse=False):
        """Find and return all files that match a given pattern.

        :param pattern: A valid regular expression.
        :param exclude: A list or tuple containing paths to exclude. Obviously,
                        you could integrate this into `pattern`, but this makes
                        it slightly easier if you have multiple paths to
                        exclude.
        :param recurse: Boolean indicating whether or not to recurse through any
                        subdirectories that are found.
        """
        matches = []
        for pth in self.listing:
            if recurse and pth.is_dir:
                pths = pth.find_all(pattern, exclude=exclude, recurse=True)
            elif not (re.match(pattern, pth.name) and
                pth.name not in exclude):
                continue
            else:
                pths = [pth]
            matches.extend(pths)
        return matches

    def glob(self, pattern, include_hidden=True, include_directories=True):
        """Equivalent of `glob.glob(path.join(pattern))`."""
        paths = list(map(path, glob.glob(self.join(pattern).absolute)))
        if not include_hidden:
            paths = [p for p in paths if not p.hidden]
        if not include_directories:
            paths = [p for p in paths if p.is_file]
        return paths

    @property
    def is_package(self):
        return '__init__.py' in self

    @property
    def listing(self):
        return [self.join(p) for p in os.listdir(self.absolute)]

    def sorted_listing(self, sort_by):
        """Return a sorted listing of a directory.

        :param sort_by: One of:

            - "mode" - Inode protection mode.
            - "ino" - Inode number.
            - "dev" - Device inode resides on.
            - "nlink" - Number of links to the inode.
            - "uid" - User id of the owner.
            - "gid" - Group id of the owner.
            - "size" -  Size in bytes of a plain file; amount of data waiting
              on some special files.
            - "atime" - Time of last access.
            - "mtime" - Time of last modification.
            - "ctime" - The "ctime" as reported by the operating system.
        """
        index = getattr(stat, sort_by.upper(), None)
        if index:
            tmp = self.listing
            tmp.sort(key=lambda i: i.stat[index])
            return tmp
        else:
            return self.listing

    @property
    def name(self):
        return os.path.split(self.absolute)[1] or '/'

    def __next__(self):
        """Iterate to the next item in the directory."""
        if self.current_index >= len(self) - 1:
            raise StopIteration
        else:
            self.current_index += 1
        return self[self.current_index]

    def __contains__(self, item):
        if isinstance(item, Path):
            item = item.name
        for path in self:
            if item == path.name:
                return True

    def __getitem__(self, index):
        return self.listing[index]

    def __iter__(self):
        return iter(self.listing)

    def __len__(self):
        return len(self.listing)


class File(Path):
    """Represents a file."""

    def copy(self, destination):
        """Copy a file."""
        destination = path(destination)
        return shutil.copy(self.absolute, destination.absolute)

    @property
    def ctime(self):
        return self.stat[9]

    @property
    def mtime(self):
        return self.stat[8]

    def read(self):
        """Return the contents of the file."""
        with self.open("r") as f:
            return f.read()

    def readlines(self):
        """Return all lines in file."""
        return self.open("r").readlines()

    def __iter__(self):
        return self.open("r").__iter__()
