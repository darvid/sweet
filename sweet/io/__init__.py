"""
    sweet.io
    ~~~~~~

    Provides an object-oriented filesystem interface and other I/O related
    libraries.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
