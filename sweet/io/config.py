"""
    sweet.io.config
    ~~~~~~~~~~~~~

    Provides various file configuration management interfaces.

    :copyright: (c) 2009-2012 David Gidwani
    :license: GPLv3
"""
from configparser import SafeConfigParser
from sweet.core.structures.dict import AttrDict


def _get_class_name(cls):
    return getattr(cls, "__name__", cls.__class__.__name__)


class NoConfigExistsError(Exception):
    pass


class ClassConfigManager(set):
    """Provides a simplistic, distributed form of configuration management.

    To use, add a `config` dict as a class attribute to all the objects for
    which you want to provide configuration for. The object's class name becomes
    the section name, and the `config` class attribute is replaced by an
    :class:`~sweet.core.structures.dict.AttrDict` containing the options from the
    actual configuration file, updated with the items in `config` as defaults,
    if they don't already exist.
    """

    def __init__(self, classes=[], strict=False, autowrite=True, **kwargs):
        """Create a new configuration manager.

        :note: All additional keyword arguments are passed to `meth:read`.

        :param strict: Whether or not to raise an exception if a class is added
                       which doesn't have a `config` attribute.
        """
        self.autowrite = autowrite
        self.config = None
        self.sources = []
        self.unsaved = False
        super(ClassConfigManager, self).__init__(classes)
        if "filenames" in kwargs or "config" in kwargs:
            self.read(**kwargs)
        for cls in classes:
            self.add(cls)

    def add(self, cls):
        """Update **cls**'s `config` attribute with a read-only dictionary
        containing the associated section in the configuration file."""
        if not hasattr(cls, "config") and not self.strict:
            return
        super(ClassConfigManager, self).add(cls)
        name = _get_class_name(cls)
        self.update_config(cls)
        config = AttrDict(self.config.items(name))
        for key, value in config.items():
            if not value:
                continue
            elif value.isdigit() or value[0] == "-" and value[1:].isdigit():
                config[key] = int(value)
            elif value.lower() in ("true", "false", "1", "0"):
                config[key] = value.lower() in ("true", "1")
        cls.config = config

    def read(self, filenames=None, config=None, config_class=SafeConfigParser):
        """Read configuration file(s) and process classes.

        :param classes: A list of classes with `config` class attributes.
        :param filenames: A list of configuration filenames to pass to the
                          configuration parser.
        :param config: A :class:`~ConfigParser.ConfigParser` instance. Only
                       required if **filenames** wasn't specified.
        :param config_class: By default, :class:`~ConfigParser.SafeConfigParser`
        """
        if filenames:
            if isinstance(filenames, str):
                filenames = [filenames]
            config = config_class()
            self.sources = config.read(filenames)
            if not self.sources:
                raise NoConfigExistsError("no configuration files found")
        else:
            assert config is not None, "must supply a configuration object"
        self.config = config
        for cls in self:
            self.update_config(cls)

    def update_config(self, cls):
        """Create any default options that don't exist in the config file."""
        if cls in self:
            section = _get_class_name(cls)
            if not self.config.has_section(section):
                self.config.add_section(section)
            for key, value in cls.config.items():
                if not self.config.has_option(section, key):
                    self.unsaved = True
                    self.config.set(section, key, str(value))

    def write(self, update=True):
        if update:
            for cls in self:
                self.update_config(cls)
        if self.unsaved and self.autowrite:
            if len(self.sources) > 1:
                # FIXME
                raise Exception("more than one config file sourced, can't "
                                "decide which one to write to!")
            elif len(self.sources) == 0:
                raise NoConfigExistsError("no file to write to")
            self.config.write(open(self.sources[0], "w"))